package main

import (
	"fmt"
	"log"
	"net/http"
	"test-webserver/counter"
)

var cnt = counter.New() 

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, you're at %q", r.URL.Path)
	})

	http.HandleFunc("/hi", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Hi");
	})

	log.Fatal(http.ListenAndServe(":25000", nil))
}
