package counter

import (
	"sync"
)

type Counter struct{
	count int
	mutex sync.Mutex
}

func New() Counter {
	return Counter{0, sync.Mutex{}}
}

/*
 * Adds a value to the counter and returns the new Value
 */
func (cnt *Counter) AddValue(value int) int {
	//Lock mutex and defer unlocking, happens after return
	cnt.mutex.Lock()
	defer cnt.mutex.Unlock()

	cnt.count += value;
	return cnt.count;
}

func (cnt *Counter) GetValue() int {
	cnt.mutex.Lock()
	defer cnt.mutex.Unlock()

	return cnt.count;
}
